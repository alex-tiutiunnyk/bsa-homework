package com.binary_studio.academy_coin;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

    private AcademyCoin() {
    }

    static class Pair {
        int a, b;

        public Pair(int a, int b) {
            this.a = a;
            this.b = b;
        }

        public int getA() {
            return a;
        }

        public int getB() {
            return b;
        }

        public void setB(int b) {
            this.b = b;
        }
    }

    static List<Pair> pairs = new ArrayList<>();

    public static int maxProfit(Stream<Integer> prices) {
        Integer sum = 0;
        boolean isNewPair = true;
        List<Integer> pricesList = prices.collect(Collectors.toList());
        for (int i = 1; i < pricesList.size(); i++) {
            int res = handle(pricesList.get(i-1), pricesList.get(i),isNewPair);
            if(res == 0)
                isNewPair = true;
            else
                isNewPair = false;
            sum += res;
        }
        return sum;
    }

    public static Integer handle(Integer a, Integer b, boolean isNewPair) {

        if (a >= b)
            return 0;
        if (pairs.size() > 0 && !isNewPair && pairs.get(pairs.size() - 1).getB() <= b) {
            int oldB = pairs.get(pairs.size() - 1).getB();
            pairs.get(pairs.size() - 1).setB(b);
            return b - oldB;
        }
        pairs.add(new Pair(a, b));
        return b-a;
    }
}
