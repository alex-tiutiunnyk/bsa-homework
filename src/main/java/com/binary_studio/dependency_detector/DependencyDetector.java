package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.List;

public final class DependencyDetector {

//    private DependencyDetector() {
//    }

    public static List<String> done = new ArrayList<>();

    public static List<String> processing = new ArrayList<>();

    public static boolean canBuild(DependencyList libraries) {
        done = new ArrayList<>();
        processing = new ArrayList<>();

        for (String lib :
                libraries.libraries) {
            if (!processLib(lib, libraries.dependencies))
                return false;
        }
        return true;
    }


    public static boolean processLib(String library, List<String[]> dependencies) {
        if (done.contains(library)) {
            return true;
        }
        if (processing.contains(library)) {
            return false;
        }
        processing.add(library);
        List<String> deps = getDep(library, dependencies);
        for (String d : deps) {
            if (!processLib(d, dependencies))
                return false;
        }
        done.add(library);
        processing.remove(library);
        return true;
    }

    public static List<String> getDep(String library, List<String[]> dependencies) {
        List<String> deps = new ArrayList<>();
        for (String[] dep : dependencies) {
            if(library.equals(dep[0])){
                deps.add(dep[1]);
            }
        }
        return deps;
    }
}
