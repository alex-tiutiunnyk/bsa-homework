package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	@Override
	public void endTurn() {
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public PositiveInteger getSize() {
		return null;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return null;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		return null;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		return null;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		return null;
	}

}
