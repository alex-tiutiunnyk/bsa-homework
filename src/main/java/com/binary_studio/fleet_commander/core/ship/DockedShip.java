package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.SubmissionPublisher;
import java.util.stream.Collectors;

public final class DockedShip implements ModularVessel {
    protected String _name;
    protected PositiveInteger _shieldHP;
    protected PositiveInteger _hullHP;
    protected PositiveInteger _powergridOutput;
    protected PositiveInteger _capacitorAmount;
    protected PositiveInteger _capacitorRechargeRate;
    protected PositiveInteger _speed;
    protected PositiveInteger _size;

    protected List<Subsystem> subsystems = new ArrayList<>();

    public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
                                       PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
                                       PositiveInteger speed, PositiveInteger size) {

        DockedShip instance = new DockedShip();
        instance._name = name;
        instance._shieldHP = shieldHP;
        instance._hullHP = hullHP;
        instance._powergridOutput = powergridOutput;
        instance._capacitorAmount = capacitorAmount;
        instance._capacitorRechargeRate = capacitorRechargeRate;
        instance._speed = speed;
        instance._size = size;

        return instance;
    }

    private void checkPossiblePowerSubsystems(Subsystem subsystem) throws InsufficientPowergridException {
        Integer totalSubsystemPG = subsystems.stream().mapToInt(subsys -> subsys.getPowerGridConsumption().value()).sum();
        if (_powergridOutput.value() - totalSubsystemPG < subsystem.getPowerGridConsumption().value())
            throw new InsufficientPowergridException(0);
    }

    @Override
    public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
        if (subsystem==null){
            subsystems.removeAll(subsystems.stream().filter(x -> x instanceof AttackSubsystem).collect(Collectors.toList()));
            return;
        }
        checkPossiblePowerSubsystems(subsystem);
        subsystems.add(subsystem);

    }

    @Override
    public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
        if (subsystem==null){
            subsystems.removeAll(subsystems.stream().filter(x -> x instanceof DefenciveSubsystem).collect(Collectors.toList()));
            return;
        }
        checkPossiblePowerSubsystems(subsystem);
        subsystems.add(subsystem);
    }

    public CombatReadyShip undock() throws NotAllSubsystemsFitted {
        if (subsystems.stream().noneMatch(subsys -> subsys instanceof AttackSubsystem)
                && subsystems.stream().noneMatch(subsys -> subsys instanceof DefenciveSubsystem))
            throw NotAllSubsystemsFitted.bothMissing();

        if (subsystems.stream().noneMatch(subsys -> subsys instanceof DefenciveSubsystem))
            throw NotAllSubsystemsFitted.defenciveMissing();

        if (subsystems.stream().noneMatch(subsys -> subsys instanceof AttackSubsystem))
            throw NotAllSubsystemsFitted.attackMissing();

        return new CombatReadyShip();
    }
}
