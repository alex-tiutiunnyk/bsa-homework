package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {
	protected String _name;
	protected PositiveInteger _powerGridRequirments;
	protected PositiveInteger _capacitorRequirments;
	protected PositiveInteger _impactReductionPercent;
	protected PositiveInteger _shieldRegeneration;
	protected PositiveInteger _hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if (name == null || name.trim().isEmpty())
			throw new IllegalArgumentException("Name should be not null and not empty");

		DefenciveSubsystemImpl instance = new DefenciveSubsystemImpl();
		instance._name = name;
		instance._powerGridRequirments = powergridConsumption;
		instance._capacitorRequirments = capacitorConsumption;
		instance._impactReductionPercent = impactReductionPercent;
		instance._shieldRegeneration = shieldRegeneration;
		instance._hullRegeneration = hullRegeneration;

		return instance;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return _powerGridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return _capacitorRequirments;
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		PositiveInteger damage;
		if(_capacitorRequirments.value() >= incomingDamage.damage.value())
			damage = incomingDamage.damage;
		else
		 damage = PositiveInteger.of((int)Math.ceil(incomingDamage.damage.value() * (1-1.0*_impactReductionPercent.value()/100)));

		return new AttackAction(damage, incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		RegenerateAction regen = new RegenerateAction(_shieldRegeneration, _hullRegeneration);
		return regen;
	}



}
