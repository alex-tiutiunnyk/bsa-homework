package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {
    protected String _name;
    protected PositiveInteger _powergridRequirments;
    protected PositiveInteger _capacitorConsumption;
    protected PositiveInteger _baseDamage;
    protected PositiveInteger _optimalSpeed;
    protected PositiveInteger _optimalSize;

    public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
                                                PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
                                                PositiveInteger baseDamage) throws IllegalArgumentException {

        if (name == null || name.trim().isEmpty())
            throw new IllegalArgumentException("Name should be not null and not empty");

        AttackSubsystemImpl instance = new AttackSubsystemImpl();
        instance._name = name;
        instance._powergridRequirments = powergridRequirments;
        instance._capacitorConsumption = capacitorConsumption;
        instance._baseDamage = baseDamage;
        instance._optimalSpeed = optimalSpeed;
        instance._optimalSize = optimalSize;

        return instance;
    }

    @Override
    public PositiveInteger getPowerGridConsumption() {
        return _powergridRequirments;
    }

    @Override
    public PositiveInteger getCapacitorConsumption() {
        return _capacitorConsumption;
    }

    @Override
    public PositiveInteger attack(Attackable target) {
        double damage = _baseDamage.value()
                * Math.min(
                        getSizeReduction(target.getSize()),
                        getSpeedReduction(target.getCurrentSpeed()));

        return PositiveInteger.of((int)Math.ceil(damage));
    }

    @Override
    public String getName() {
        return _name;
    }

    public double getSizeReduction(PositiveInteger targetSize) {
        if (targetSize.value() >= _optimalSize.value()) {
            return 1;
        }
        return 1.0 * targetSize.value() / _optimalSize.value();
    }

    public double getSpeedReduction(PositiveInteger targetSpeed) {
        if (targetSpeed.value() <= _optimalSpeed.value()) {
            return 1;
        }
        return 1.0 * _optimalSpeed.value() / (2 * targetSpeed.value());
    }
}
